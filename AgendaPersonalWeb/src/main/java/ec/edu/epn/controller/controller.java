package ec.edu.epn.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ec.edu.epn.entities.Correo;
import ec.edu.epn.entities.Persona;
import ec.edu.epn.entities.Telefono;
import ec.edu.epn.model.PersonaDAO;

@WebServlet("/controller")
public class controller extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public controller() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String nombre = request.getParameter("nombre");
		String apellido = request.getParameter("apellido");
		String direccion = request.getParameter("direccion");
		String direccionCorreo = request.getParameter("correo");
		String numeroTelefono = request.getParameter("telefono");

		Persona persona = new Persona(nombre, apellido, direccion);

		List<Correo> correos = new ArrayList<Correo>();
		Correo correo = new Correo(direccionCorreo, persona);
		correos.add(correo);

		List<Telefono> telefonos = new ArrayList<Telefono>();
		Telefono telefono = new Telefono(numeroTelefono, persona);
		telefonos.add(telefono);

		PersonaDAO personaDAO = new PersonaDAO();
		persona.setCorreos(correos);
		persona.setTelefonos(telefonos);
		personaDAO.create(persona);
		
	}
	
}

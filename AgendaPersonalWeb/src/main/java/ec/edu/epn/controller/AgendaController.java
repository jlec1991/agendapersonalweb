package ec.edu.epn.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import ec.edu.epn.entities.Correo;
import ec.edu.epn.entities.Persona;
import ec.edu.epn.entities.Telefono;
import ec.edu.epn.model.PersonaDAO;

@ManagedBean(name = "agendaController")
@ViewScoped
public class AgendaController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String nombre;
	private String apellido;
	private String direccion;
	private String telefono;
	private String correo;

	public void guardarRegistroEnBDD() {
		Persona persona = new Persona(nombre, apellido, direccion);
		List<Telefono> telefonos = new ArrayList<>();
		telefonos.add(new Telefono(telefono, persona));
		List<Correo> correos = new ArrayList<Correo>();
		correos.add(new Correo(correo, persona));
		PersonaDAO personaDAO = new PersonaDAO();
		personaDAO.create(persona);
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

}

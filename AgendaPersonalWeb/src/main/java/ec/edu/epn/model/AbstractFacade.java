package ec.edu.epn.model;

import javax.persistence.EntityManager;

import ec.edu.epn.factories.EMF;

public abstract class AbstractFacade<T> {

	private Class<T> entityClass;

	protected EntityManager em = EMF.createEntityManager();

	public AbstractFacade(Class<T> entityClass) {
		this.entityClass = entityClass;
	}

	public void create(T entity) {
		try {
			em.getTransaction().begin();
			em.persist(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al persistir la entidad: " + entity.getClass().toString()
					+ " Error: " + e.getMessage());
		} finally {
			em.close();
		}

	}

	public void edit(T entity) {
		try {
			em.getTransaction().begin();
			em.merge(entity);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al modificar la entidad: " + entity.getClass().toString()
					+ " Error: " + e.getMessage());
		} finally {
			em.close();
		}

	}

	public void remove(T entity) {
		try {
			em.getTransaction().begin();
			em.remove(em.merge(entity));
			em.getTransaction().commit();
		} catch (Exception e) {
			System.err.println("Ha ocurrido un error al eliminar la entidad: " + entity.getClass().toString()
					+ " Error: " + e.getMessage());
		} finally {
			em.close();
		}

	}

	public T find(Object id) {
		T entity = null;
		try {
			entity = em.find(entityClass, id);
		} catch (Exception e) {
			System.err.println(
					"Ha ocurrido un error al buscar la entidad: " + id.toString() + " Error: " + e.getMessage());
		} finally {
			em.close();
		}
		return entity;
	}
}

package ec.edu.epn.model;

import ec.edu.epn.entities.Persona;

public class PersonaDAO extends AbstractFacade<Persona> {

	public PersonaDAO() {
		super(Persona.class);
	}
}

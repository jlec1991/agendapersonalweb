package ec.edu.epn.model;

import ec.edu.epn.entities.Correo;

public class CorreoDAO extends AbstractFacade<Correo> {

	public CorreoDAO() {
		super(Correo.class);
	}
}

package ec.edu.epn.model;

import ec.edu.epn.entities.Telefono;

public class TelefonoDAO extends AbstractFacade<Telefono> {

	public TelefonoDAO() {
		super(Telefono.class);
	}
}
